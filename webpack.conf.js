// import path from 'path'
// import HtmlWebpackPlugin from 'html-webpack-plugin'
// import MiniCSSExtractPlugin from 'mini-css-extract-plugin'
require('dotenv')
  .config({ path: `.env.${process.env.WEBPACK_MODE}` })
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const config = {
  mode: 'development',
  entry: path.resolve(__dirname, './src/script.js'),
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
  },
  devtool: 'source-map',
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, './src/index.html'),
      minify: true,
    }),
    new HtmlWebpackPlugin({
      filename: 'thank.html',
      template: path.resolve(__dirname, './src/thank.html'),
      minify: true,
    }),
    new MiniCSSExtractPlugin({
      filename: '[name].[contenthash].css',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.html$/i,
        use: ['html-loader'],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCSSExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.js$/i,
        exclude: /node_modules/,
        use: [
          'babel-loader',
        ],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        use: [
          {
            options: {
              name: "[name].[ext]",
              outputPath: "static/"
            },
            loader: "file-loader"
          }
        ]
      },
    ],
  },
}

if (process.env.NODE_ENV === 'development') {
  config.devServer = {
    host: '0.0.0.0',
    port: process.env.PORT,
    contentBase: './dist',
    watchContentBase: true,
    open: true,
    https: false,
    useLocalIp: true,
    disableHostCheck: true,
    overlay: true,
    noInfo: true,
    after: function (app, server, compiler) {
      const port = server.options.port
      const https = server.options.https ? 's' : ''
      const env = app.settings.env
      const localIp = process.env.LOCAL_IP
      const domain1 = `http${https}://${localIp}:${port}`
      const domain2 = `http${https}://localhost:${port}`
      const infoColor = message => {
        return `\u001b[1m\u001b[34m${message}\u001b[39m\u001b[22m`
      }

      console.log(`Mode : ${env}.\nProject running at:\n  - ${infoColor(domain1)}\n  - ${infoColor(domain2)}`)
    },
  }
} else if (process.env.NODE_ENV === 'production') {
  config.plugins.push(new CleanWebpackPlugin())
}

module.exports = config
