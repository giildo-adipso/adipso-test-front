# Projet ADISPO

## Utilisation des maquettes

Installer les dépendances
```
yarn install
```

Pour lancer un serveur en local
```
yarn run dev
```

Pour compiler le code dans le cadre d'une mise en production
```
yarn run build
```  

...et transférer les fichiers du dossier "./dist/" sur le serveur de production.

## Questions techniques

- Sur une liste d'éléments, quelle est la différence entre les actions de filtrer et de trier ?

Filtrer des éléments permet de n'afficher que ce qui est nécessaire, alors que trier permet de mettre en ordre l'intégralité des données.

- Un chef de projet vous demande de faire une estimation du ticket (tâche) ci-dessous qui concerne le site vitrine d’un
  de nos clients. Comment procédez-vous ?

__Tâche 1__ : Il est nécessaire de stocker en base, avec la photo, si le picto est présent ou non.  
__Tâche 2__ : modifier l'interface d'insertion des photos pour y ajouter un "_switch button_".  
__Tâche 3__ : Modifier l'affichage des photos, pour afficher le picto si la valeur est à "_true_".

Estimation une demi journée pour l'ensemble.

- Un webdesigner vous présente fièrement une animation et vous demande si elle est réalisable. L’animation est
  techniquement réalisable mais à première vue celle-ci sera gourmande et longue à mettre en place. Comment abordez-vous
  le sujet ?

Si l'animation peut apporter un vrai plus à l'application, mais que ni les ressources machines, ni le temps imparti pour le projet ne sont en adéquation avec cette animation. Ne peut-on pas trouver une solution intermédiaire : en simplifiant en partie l'animation, en ne prenant qu'une partie... Si le graphiste pense que c'est une bonne chose pour le projet, il est important de trouver un terrain d'entente.

## Logique

- Le chiffre pourrait être 4 : 6 * 4 - 1 = 23

- C : mais je trouve pas ça totalement logique pour moi les deux ronds : pères et veufs devraient être complètement dans hommes et se recouper comme ils font là, car tous les pères sont des hommes et tous les veufs sont des hommes, mais tous les hommes ne sont pas pères et tous les hommes ne sont pas veufs et seulement une partie des veufs sont pères.

## Récapitulatif

J'ai malheureusement perdu 1h30 au départ car je voulais utiliser photoshop sur mon ordi = windows = pas tous mes outils que j'ai sur Linux... quand je voyais que je n'avançais quasiment pas, je suis repassé sur Linux avec un autre ordi sur windows à côté.  

Sinon l'ensemble du projet n'est pas compliqué en soi, je n'avais juste pas la police "Helvetica", j'ai essayé de trouver un équivalent, qui ressemble un maximum, sur Google font.

Pour que vous puissiez un peu mieux estimer le temps j'ai fait les commit à la fin de la première page, à la fin du smartphone pour la seconde page et à la fin du projet, sans celui que j'ai perdu au départ.

Merci pour tout, en espérant vivement que le projet vous plaira !
